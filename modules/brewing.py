#!/usr/bin/python

import numpy as np
from matplotlib.widgets import Slider, Button
import matplotlib.pyplot as plt

def get_extract_potential(type):

    # See table 5.1
    # Units in Gravity Units

    extract_potentials = {'chocolate': 27.5,
                          'crystal' : 34.,
                          'munich' : 35.5,
                          'pale' : 36.,
                          }

    return extract_potentials['pale']

def calc_water_volume(batch_size=5.0, boil_time=1.0, spent_grain=10.0):
    ''' Batch size in gallons
    boil_time in hours
    spent grain in pounds
    '''

    # data from table 8.1
    mash_weight = np.arange(3, 25)

    # page 64
    def calc_water_loss(spent_grain):
        water_loss = spent_grain * 0.2
        return water_loss

    # For hop debris losses and sludge after primary fermatation
    batch_size += 0.5

    # shrinkage during cooling time
    shrinkage = 0.96
    batch_size /= shrinkage


    # Evaporation = 5% per hour
    evap_rate = 0.05 # page 65, paragraph 0
    batch_size /= (1 - evap_rate)


    # Runoff volume
    runoff_volume = batch_size
    equipment_losses = 1.0 # gallons

    water_loss = calc_water_loss(spent_grain)

    total_volume = runoff_volume + equipment_losses + water_loss

    # determine amount of water for mashing and sparging
    mash_volume = 0.3325 * spent_grain
    sparge_volume = total_volume - mash_volume

    print('\nWater bill')
    print('Total volume\t {:.2f}'.format(total_volume))
    print('Mash volume\t {:.2f}'.format(mash_volume))
    print('Sparge volume\t {:.2f}'.format(sparge_volume))

def calc_hop_bill(target_IBU=30., volume=5.5, gravity_boil=1.050,
        boil_time=60.0, alpha_acid=0.06, pellet=True):

    '''
    volume in gallons
    gravity_boil in specific gravity units
    boil_time in minutes
    alpha_acid as a fraction

    returns the weight of the hop in ounces
    '''

    # Correction of high gravity worts which change the utilization
    if gravity_boil > 1.050:
        gravity_correction = 1 + ((gravity_boil - 1.050) / 0.2)
    else:
        gravity_correction = 1.0

    # Get utilization value
    boil_data = np.array([0.0, 4.5, 14.5, 24.5, 37.0, 52.0, 67.0, 75])
    util_whole = np.array([0, 5, 12, 15, 19, 22, 24, 27])
    util_pellet = np.array([0, 6, 15, 19, 24, 27, 30, 34])

    if pellet:
        util_data = util_pellet
    else:
        util_data = util_whole

    utilization = np.interp(boil_time, boil_data, util_data)

    if utilization == 0.0:
        return np.NaN

    # page 81, hops in ounces
    hop_weight = volume * gravity_correction * target_IBU / \
                 (utilization * alpha_acid * 7489.)

    return hop_weight

def calc_hop_bill(hops, target_IBU):


    # Create figure
    fig, ax = plt.subplots()
    ax.set_xlim((0, 1))
    ax.set_ylim((0, 1))

    update.sliders = [add_new_slider(fig, hops, hop, update) for hop in hops]

    update.hops = hops

    add_finish_button(fig)

    ax.text(0.5, 0.5, 'Target IBU = {:.1f}'.format(target_IBU),)

    current_IBU = get_IBU(hops)

    #global IBU_text

    update.IBU_text = ax.text(0.5, 0.1, 'Current IBU = ' + \
            '{:.1f}'.format(current_IBU))

    plt.subplots_adjust(bottom=add_new_slider.count*0.25)
    plt.show()

def get_IBU(hops):
    IBU = 0
    for hop in hops:
    	IBU += hops[hop]['weight'] * hops[hop]['alpha_acid']

    return IBU

def add_new_slider(fig, hops, hop_name, update):
    if not hasattr(add_new_slider, 'count'):
    	add_new_slider.count = 0
    add_new_slider.count +=  1

    ax = fig.add_axes([0.25, add_new_slider.count*0.1, 0.5, 0.05])
    #global s
    s = Slider(ax, hop_name, 0.0, 10.0, valinit=hops[hop_name]['weight'])
    s.on_changed(update)
    return s

def add_finish_button(fig):
    return None

def button_callback(e):
    return None

def update(val):
    for s in sliders:
        s.val
    IBU = get_IBU(update.hops)

    update.IBU_text.set_text('Current IBU = {:.1f}'.format(update.slider.val))
    plt.draw()

def main():
    hops = {'saaz' : {'weight' : 1.00,
                      'alpha_acid' : 0.036,
                      'boil_time' : 5.0,
                      },
            'perle' : {'weight' : 0.5,
                       'alpha_acid' : 0.08,
                       'boil_time' : 20.0,
                       },
            'perle' : {'weight' : 0.5,
                       'alpha_acid' : 0.08,
                       'boil_time' : 60.0,
                       }
            }

    # 2) Determine amount of flavor and aroma hops
    hops = calc_hop_bill(hops, 25,
                          )


if __name__ == '__main__':
	main()


