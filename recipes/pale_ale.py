#!/usr/bin/python

'''

Sierra Nevada Pale Ale characteristics:

Malts
   2-row
   caramel

OG      13.0

FG      4.0

IBUs    23

'''

import brewing

# First we calculate the malt bill
# --------------------------------

# Step 1: determine target gravity
target_gravity = 40 # GU

# Step 2: determine volume
volume = 5.5 # gallons

# Step 3: determine fermentable ingredients and proportions
malts = {'2row' : {'fraction' : 1,
                   'type' : 'pale',
                   },
         }

# Step 4: Determine efficiency of extraction
efficiency = 0.68

# Determine Grain Bill
# --------------------

# 1) Determine the total amount of extract needed
# extract_total = volume [gallons] * gravity [GU]
extract_total = volume * target_gravity

# 2) Calculate gravity contribution from each fermentable
for malt in malts:
    malts[malt]['gravity'] = malts[malt]['fraction'] * extract_total

# 3) Calculate weight of each fermentable
print('Grain bill')

grain_weight = 0.0
gravity_wort = 0.0

for malt in malts:
    extract_potential = brewing.get_extract_potential(malts[malt]['type'])

    malts[malt]['weight'] = malts[malt]['gravity'] / extract_potential / \
                            efficiency

    grain_weight += malts[malt]['weight']

    gravity_wort += malts[malt]['gravity']

    print '{:s}\t{:.2f} lbs'.format(malt, malts[malt]['weight'])

print('\nWort Gravity\t{:.2f} GU'.format(gravity_wort))

# Determine Hops Bill
# -------------------

# 1) Determine IBUs
IBUs = 25.0 # mg / L

# using magnum perle and cascade

hops = {'amarillo' : {'weight' : 0.5,
                   'alpha_acid' : 0.075,
                   'boil_time' : 5.0,
                   },
        'amarillo' : {'weight' : 0.5,
                    'alpha_acid' : 0.075,
                   'boil_time' : 20.0,
                  },
        'amarillo' : {'weight' : 1.00,
                   'alpha_acid' : 0.075,
                   'boil_time' : 60.0,
                  }
        }

# 2) Determine amount of flavor and aroma hops
# hops = brewing.calc_hop_bill(target_IBU=IBUs,
#                      volume=volume, # gallons
#                      gravity_boil=target_gravity,
#                      )

# 3) Determine IBUs contributed by flavor hops

# 4) Determine quantity of bittering hops needed

print('\nHop bill')
for hop in hops:
    print('{:s}\t{:.2f} oz'.format(hop, hops[hop]['weight']))

# Determine Water Amount
# ----------------------

water_volume = brewing.calc_water_volume(batch_size=volume,
                                         boil_time=1.0,
                                         spent_grain=grain_weight)

# Used the following recipe:

# Grain bill
# 2row	    7 lbs
# caramel   1.25 lbs
#
# Hops          Amount      Boil Time
# magnum	0.33 oz     60 min
# perle	        0.50 oz     30 min
# cascade	1.00 oz     10 min
# cascade	2.00 oz     2 min
#
# Water bill
# Mash volume	 2.75
# Sparge volume	 6
#
# OG : 31 GU
# Sparge time : 45 min at 155 deg F
#
# Mashed on 2014/07/06


